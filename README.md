# Workstation Setup

Setup tools for developers

## Manual works

1. Install Ansible
```bash
sudo apt update
```

```bash
sudo apt install ansible unzip -y
```

2. You might need these commands:
```bash
unzip ansible  && mv ansible /usr/local/bin
```

3. Apply the configuration
```bash
ansible-playbook devtools.yml --ask-become-pass
```
___

# License
MIT

Author Information
------------------

Author: Miltex

[LinkedIn](https://www.linkedin.com/in/flmilton/)
