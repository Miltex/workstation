---
- name: Installing Apps
  become: true
  apt:
    name: '{{ item }}'
    install_recommends: yes
    state: present
  loop:
      - vim
      - htop
      - curl
      - wget
      - tree
      - apt-transport-https
      - ca-certificates
      - python3-pip
      - git
      - bash-completion
      - zsh
      - flameshot
      - fonts-hack
      - tilix
      - virtualbox
      - krita
      - openssh-client
      - openjdk-8-jdk
      - pipenv

- block:
  - name: Verify if Oh-My-zsh is installed
    command: test -d /home/{{ ansible_user_id }}/.oh-my-zsh
    register: ohmyzsh
    ignore_errors: true
  - name: Installing Oh-My-zsh
    shell:
      cmd: 'curl -fsL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | bash'
      warn: false
    when: ohmyzsh.rc != 0
  - name: Changing Default Shell to ZSH
    become: yes
    user: 
      name: '{{ ansible_user_id }}'
      shell: /bin/zsh 
  - name: Changing Default ZSH Theme to Agnoster
    lineinfile:
      path: /home/{{ ansible_user_id }}/.zshrc
      regexp: '^ZSH_THEME='
      line: 'ZSH_THEME="agnoster"'
  - name: Creating ZSH Completion folder
    file:
      path: /home/{{ ansible_user_id }}/.oh-my-zsh/completions
      state: directory
      mode: 0755
  - name: Adding autoload to compinit
    lineinfile:
      path: /home/{{ ansible_user_id }}/.zshrc
      line: 'autoload -U compinit && compinit'
      state: present

- name: Installing AWS CLI via pip3
  become: true
  pip:
    name: awscli
    executable: pip3

- block:
  - name: Install DBearver Key
    become: true
    apt_key: 
      url: 'https://dbeaver.io/debs/dbeaver.gpg.key'
      state: present
  - name: Install DBearver Repository
    become: true
    apt_repository:
      repo: 'deb [arch=amd64] https://dbeaver.io/debs/dbeaver-ce /'
      state: present
      filename: dbeaver-ce
  - name: Install DBeaver CE
    become: true
    apt:
      name: dbeaver-ce
      force_apt_get: yes
      update_cache: yes

- block:
  - name: Install Google Key
    become: true
    apt_key: 
      url: 'https://dl.google.com/linux/linux_signing_key.pub'
      state: present
  - name: Install Google Repository
    become: true
    apt_repository:
      repo: 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main'
      state: present
      filename: google-chrome
  - name: Install Google Chrome
    become: true
    apt:
      name: google-chrome-stable

- block:
  - name: Installing Vagrant 2.2.15
    become: true
    unarchive:
      src: 'https://releases.hashicorp.com/vagrant/2.2.15/vagrant_2.2.15_linux_amd64.zip'
      dest: /usr/local/bin
      remote_src: yes

- block:
  - name: Installing Terraform 0.15.0
    become: true
    unarchive:
      src: 'https://releases.hashicorp.com/terraform/0.15.0/terraform_0.15.0_linux_amd64.zip'
      dest: /usr/local/bin
      remote_src: yes

- block:
  - name: Installing Vault 1.7.1
    become: true
    unarchive:
      src: 'https://releases.hashicorp.com/vault/1.7.1/vault_1.7.1_linux_amd64.zip'
      dest: /usr/local/bin
      remote_src: yes

- block:
  - name: Installing gomplate 3.7.0
    become: true
    get_url:    
      url: 'https://github.com/hairyhenderson/gomplate/releases/download/v3.7.0/gomplate_linux-amd64'
      dest: /usr/local/bin/gomplate
      mode: 755
      remote_src: yes

- block:
  - name: Installing Stern 1.11.0
    become: true
    get_url:    
      url: 'https://github.com/wercker/stern/releases/download/1.11.0/stern_linux_amd64'
      dest: /usr/local/bin/stern
      mode: 755
      remote_src: yes

- block:
  - name: Install Google Cloud SDK Key
    become: true
    apt_key:
      url: 'https://packages.cloud.google.com/apt/doc/apt-key.gpg'
      state: present
  - name: Install Google Cloud SDK Repository
    become: true
    apt_repository:
      repo: 'deb [arch=amd64] https://packages.cloud.google.com/apt cloud-sdk main'
      state: present
      filename: google-cloud-sdk
  - name: Install Google Cloud SDK
    become: true
    apt:
      name: google-cloud-sdk

- block: 
  - name: Install Docker Key
    become: true
    apt_key:
      url: 'https://download.docker.com/linux/ubuntu/gpg'
      state: present
  - name: Install Docker Repository
    become: true
    apt_repository:
      repo: 'deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} stable'
      state: present
      filename: docker-ce
  - name: Install Docker
    become: true
    apt:
      name: docker-ce
  - name: Adding existing user to group Docker
    become: yes
    user: 
      name: '{{ ansible_user_id }}'
      groups: docker 
      append: yes
  - name: Install Docker-Compose
    become: true
    get_url:
      url: 'https://github.com/docker/compose/releases/download/1.26.2/docker-compose-Linux-x86_64'
      dest: '/usr/local/bin/docker-compose'
      mode: 755
      remote_src: yes
  - name: Install Docker Machine Bash Completion
    become: true
    get_url:
      url: 'https://raw.githubusercontent.com/docker/machine/v0.16.0/contrib/completion/bash/docker-machine.bash'
      dest: '/etc/bash_completion.d/docker-machine'
      remote_src: yes
  - name: Install Docker-Compose Bash Completion
    become: true
    get_url:
      url: 'https://raw.githubusercontent.com/docker/compose/1.26.2/contrib/completion/bash/docker-compose'
      dest: '/etc/bash_completion.d/docker-compose'
      remote_src: yes
  - name: Install Docker-Compose and Docker Machine Zsh Completion
    lineinfile:
      path: /home/{{ ansible_user_id }}/.zshrc
      regexp: '^plugins='
      line: 'plugins=(git docker docker-compose)'
    
- block:
  - name: Install Minikube
    become: true
    get_url:
      url: 'https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64'
      dest: '/usr/local/bin/minikube'
      mode: 755
      remote_src: yes
  - name: Installing Kubectl 1.21.0
    become: true
    get_url:    
      url: 'https://dl.k8s.io/release/v1.21.0/bin/linux/amd64/kubectl'
      dest: /usr/local/bin/kubectl
      mode: 755
      remote_src: yes          
  - name: Downloading Kubectx and Kubens 
    become: true
    git:
      repo: 'https://github.com/ahmetb/kubectx'
      dest: /opt/kubectx 
  - name: Creating Symlink to kubectx and kubens 
    become: true
    file:
      src: '/opt/kubectx/{{ item }}'
      dest: '/usr/local/bin/{{ item }}'
      state: link
    with_items:
      - kubectx
      - kubens  
  - name: Creating ZSH Completion  
    file:
      src: '/opt/kubectx/completion/{{ item }}'
      dest: '/home/{{ ansible_user_id }}/.oh-my-zsh/completions/_{{ item }}'
      state: link
    with_items:
      - kubectx.zsh
      - kubens.zsh 
      
- block:
  - name: Adding Longsleep/Golang Backports Repository
    become: true
    apt_repository:
      repo: 'ppa:longsleep/golang-backports'
  - name: Install Golang 
    become: true
    apt:
      name: golang-go

- block:
  - name: Adding Peek Repository
    become: true
    apt_repository:
      repo: 'ppa:peek-developers/stable'
  - name: Install Peek
    become: true
    apt:
      name: peek

- block: 
  - name: Install Brave Key
    become: true
    apt_key:
      url: 'https://brave-browser-apt-release.s3.brave.com/brave-core.asc'
      state: present
  - name: Install Brave Repository
    become: true
    apt_repository:
      repo: 'deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main'
      state: present
      filename: brave
  - name: Install Brave Browser
    become: true
    apt:
      name: brave-browser

- block:
  - name: Adding Ulauncher Repository
    become: true
    apt_repository:
      repo: 'ppa:agornostal/ulauncher'
  - name: Install Ulauncher
    become: true
    apt:
      name: ulauncher

- block:
  - name: Installing simplescreenrecorder
    become: true
    apt:
      name: simplescreenrecorder
      state: present
      force_apt_get: yes
      update_cache: yes

- block:
  - name: Install Microsoft Key
    become: true
    apt_key:
      url: 'https://packages.microsoft.com/keys/microsoft.asc'
      state: present
  - name: Install VSCode Repository
    become: true
    apt_repository:
      repo: 'deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main'
      state: present
      filename: vscode
  - name: Install Visual Studio Code
    become: true
    apt:
      name: code
  - name: Install Visual Studio Code Extensions
    shell:
      cmd: code --install-extension '{{ item }}' 
    loop: 
      - ms-python.python
      - ms-azuretools.vscode-docker
      - bbenoist.vagrant
      - hashicorp.terraform
      - gruntfuggly.todo-tree
      - njpwerner.autodocstring
      - eamodio.gitlens
      - coenraads.bracket-pair-colorizer
      - ms-azuretools.vscode-docker
      - dracula-theme.theme-dracula
      - alphabotsec.vscode-eclipse-keybindings
      - janisdd.vscode-edit-csv
      - mhutchie.git-graph
      - hashicorp.terraform
      - ms-kubernetes-tools.vscode-kubernetes-tools